#include <stdio.h>
#include <stdlib.h>

int main()
{
    //DECLARATIONS
   int nb;
   float note, moyenne;
   int cpt;
   float som;
   float noteMin, noteMax;


   //INIT
   som=0;
   noteMin=20;
   noteMax=0;
    //CORPS
   printf("Quel est le nombre d'eleves ? ");
   scanf("%d", &nb);

   for(cpt=0; cpt < nb; cpt++)
   {
       printf("\nQuelle est la note (0-20) ?");

       do
       {
           scanf("%f", &note);

           if (note < 0 || note > 20)
           {
               printf("\nNote Invalide !! Entre 0 et 20 !!!\nVeuillez resaisir : ");
           }

       }while(note < 0 || note > 20);

       if (note >= 10)
       {
           printf("\nAdmis(e) avec la note de %05.2f", note);
       }
       else
       {
           printf("\nEchec avec la note de %05.2f", note);
       }

        if (note > noteMax)
       {
            noteMax = note;
       }
       else
       {
            if (note < noteMin)
            {
                noteMin = note;
            }
       }



       som+=note;
   }

   if (nb > 0)
   {
        moyenne = som / nb;
        printf("\n\nMoyenne de la classe :  %05.2f", moyenne);
        printf("\nNote Min : %05.2f\nNote Max : %05.2f", noteMin, noteMax);
   }
   else
   {
       printf("\nAucun eleve dans la classe !!");
   }
    return 0;
}
